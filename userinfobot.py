import praw
import random

class User:
    def __init__(self, user):
        self.name = user
        self.favorite_words = {}
        self.facts = []

    def get_random_fact():
        return random.choice(self.facts)

    def get_random_facts():
        return random.sample(self.facts, 5)

class Bot:
    def __init__(self):
        self.user_agent = "UserInfoBot v1.0"
        self.reddit = praw.Reddit(self.user_agent)
        self.users_searched = []
    
    def search_user(self, user):
        self.reddit.get_redditor(user)

def main():

    user = input("Enter a username: ")

    bot = Bot()
    bot.search_user(user)

if __name == "__main__":
    main()

