This is a learning project that uses the PRAW library. The script will 
prompt the user to enter a username. Once a name is entered, the script 
will look up statistics about the user (common 'interesting' words, 
facts, etc.). The script may eventually search autonomously in the 
future.
